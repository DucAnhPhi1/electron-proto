// @flow
import React, { Component } from 'react';
import Tree from 'react-animated-tree';
import { Terminal } from 'xterm';
import { FitAddon } from 'xterm-addon-fit';
import * as os from 'os';
import * as pty from 'node-pty';

const { dialog, app } = require('electron').remote;
const fs = require('fs');
const PDFDocument = require('pdfkit');

type Props = {};

export default class HomeView extends Component<Props> {
  props: Props;

  openInEditor = () => {
    const shell = process.env[os.platform() === 'win32' ? 'COMSPEC' : 'SHELL'];
    const ptyProcess = pty.spawn(shell, [], {
      name: 'xterm-color',
      cols: 80,
      rows: 30,
      cwd: process.cwd(),
      env: process.env
    });

    ptyProcess.on('data', data => {
      process.stdout.write(data);
    });

    ptyProcess.write('code test.py\r');
  };

  savePDF = () => {
    const options = {
      title: 'Save PDF file',
      defaultPath: `${app.getPath('downloads')}/summary.pdf`
    };
    dialog.showSaveDialog(null, options, path => {
      const doc = new PDFDocument();
      doc.pipe(fs.createWriteStream(path));
      doc.text('Report summary', 100, 100);
      doc.end();
      console.log(path);
    });
  };

  openTerminal = () => {
    // Initialize node-pty with an appropriate shell
    const shell = process.env[os.platform() === 'win32' ? 'COMSPEC' : 'SHELL'];
    const ptyProcess = pty.spawn(shell, [], {
      name: 'xterm-color',
      cols: 80,
      rows: 30,
      cwd: process.cwd(),
      env: process.env
    });

    // Initialize xterm.js and attach it to the DOM
    const xterm = new Terminal();
    const fitAddon = new FitAddon();
    xterm.loadAddon(fitAddon);
    xterm.open(document.getElementById('terminal'));

    // Make the terminal's size and geometry fit the size of #terminal
    fitAddon.fit();

    // Setup communication between xterm.js and node-pty
    xterm.onData(data => ptyProcess.write(data));
    ptyProcess.on('data', data => {
      xterm.write(data);
    });
    ptyProcess.write('code .\r');
  };

  render() {
    return (
      <div>
        <h1>Homepage</h1>
        <Tree
          content="Table of Contents"
          type="Overview"
          open
          visible
          onClick={console.log}
        >
          <Tree content="Project1" open>
            <Tree content="Python Program" type="Actions">
              <button type="button" onClick={this.openInEditor}>
                open in editor
              </button>
              <button type="button" onClick={this.savePDF}>
                generate PDF
              </button>
              <button type="button" onClick={this.openTerminal}>
                run program
              </button>
            </Tree>
            <Tree content="SAS Program" />
            <Tree content="Shell Script" />
          </Tree>
          <Tree content="Project2" open>
            <Tree content="Python Program" />
            <Tree content="SAS Program" />
            <Tree content="Shell Script" />
          </Tree>
        </Tree>
      </div>
    );
  }
}
