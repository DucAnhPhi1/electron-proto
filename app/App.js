// @flow
import React, { Component } from 'react';
import { Provider } from 'react-redux';
import { Switch, Route } from 'react-router';
import { ConnectedRouter } from 'connected-react-router';
import HomeView from './views/home';
import { configureStore, history } from './store/configureStore';

const store = configureStore();

type Props = {};

export default class App extends Component<Props> {
  props: Props;

  render() {
    return (
      <Provider store={store}>
        <ConnectedRouter history={history}>
          <Switch>
            <Route path="/" component={HomeView} />
          </Switch>
        </ConnectedRouter>
      </Provider>
    );
  }
}
